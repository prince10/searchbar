//
//  ViewController.m
//  serchbar
//
//  Created by Prince on 05/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSArray *data;
    NSArray *searchResult;
    
}

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar1;
@property (strong, nonatomic) IBOutlet UITableView *tableView1;

@end

@implementation ViewController
@synthesize tableView1;

- (void)viewDidLoad {
    
   data=@[@"one",@"two",@"three",@"four",@"five",@"true",@"dfsdf",@"om",@"then",@"thing",@"dead",@"for",@"fine",@"fit",@"on",@"twelve",@"might",@"mike",@"mine",@"not",@"non",@"no"];
    searchResult = data;
   [ tableView1 reloadData];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return searchResult.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"id"];
    cell.textLabel.text=searchResult[indexPath.row];
    return cell;
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    
    NSArray *results = [data filteredArrayUsingPredicate:resultPredicate];// used to search the data form original data;
    searchResult=[NSArray arrayWithArray:results];    //store the search data in the empty array named as seachresult;
   [ tableView1 reloadData];                                 //table view is shown
    
    if (searchResult.count==0) {
        
        searchResult=[NSArray arrayWithArray:data];
        
    }
    [tableView1 reloadData];

//    else(searchResult.text.length>=0)
//    {
//        searchResult=[NSArray arrayWithArray:data];
//    }

}

    
    



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
