//
//  main.m
//  serchbar
//
//  Created by Prince on 05/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
