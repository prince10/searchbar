//
//  ViewController.h
//  serchbar
//
//  Created by Prince on 05/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>


@end

